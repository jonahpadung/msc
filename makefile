PYTHON=python
path=examples
recursive=True

make:
	@echo Installing jpc...
	${PYTHON} setup.py install

clean-local:
	@echo removing local compiled files
	rm jpc/*.c jpc/*.html jpc/*.cpp

clean:
	@echo removing all compiled files
	${PYTHON} setup.py clean
	rm jpc/*.c jpc/*.html

env:
	@echo creating conda environment...
	conda env create --file environment.yml
	# conda activate jpc
	@echo use make to install jpc

test:
	@echo testing jpc...
	cd tests && python shortTests.py

nbtest:
	@echo testing example notebooks...
	@echo test $(path)
	cd tests && python notebookTests.py --path $(path) --recursive $(recursive)
