import  numpy as np
cimport numpy as np
cimport cython
from libc.math cimport sqrt, pow, log
from cython.parallel import prange
cdef double PI = 3.1415926535

DTYPE   = np.float
DTYP1   = np.int32
ctypedef np.float_t DTYPE_t 


@cython.boundscheck(False)
@cython.cdivision(True)
@cython.nonecheck(False)
@cython.wraparound(False)
cpdef MSD2d(double [:] d2, double [:] xt, double [:] yt, int Nt,  int tdev):
    """
    Mean square displacement in two dimensions 
    xt  : time series of x-coordinate
    yt  : time series of y-coordinate
    d2  : square of displacement 
    Nt  : total number of data points 
    tdev: time difference

    returns: updates the dr2 vector
    """
    cdef int i, j
    cdef double dr2, dx, dy
    
    for i in range(tdev): 
        dr2=0
        for j in range(Nt-i-1):
            dx = xt[i+j+1] - xt[j]
            dy = yt[i+j+1] - yt[j]
            dr2 += dx*dx + dy*dy 
        d2[i] = dr2/(Nt-i-1)
    return 




@cython.boundscheck(False)
@cython.cdivision(True)
@cython.nonecheck(False)
@cython.wraparound(False)
cpdef MSD3d(double [:] d2, double [:] xt, double [:] yt,  double [:] zt, int Nt,  int tdev):
    """
    Mean square displacement in two dimensions 
    xt  : time series of x-coordinate
    yt  : time series of y-coordinate
    zt  : time series of y-coordinate
    d2  : square of displacement 
    Nt  : total number of data points 
    tdev: time difference

    returns: updates the dr2 vector
    """
    cdef int i, j
    cdef double dr2, dx, dy, dz
    
    for i in range(tdev): 
        dr2=0
        for j in range(Nt-i-1):
            dx = xt[i+j+1] - xt[j]
            dy = yt[i+j+1] - yt[j]
            dz = zt[i+j+1] - zt[j]
            dr2 += dx*dx + dy*dy + dz*dz
        d2[i] = dr2/(Nt-i-1)
    return 
